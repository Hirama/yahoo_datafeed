var express = require('express')
var app = express()
const ev = require('./EventListener')

const _ = require('lodash')

ev.subscriber()


const tokens = new Map()
_.each([
  { "addr": "0x519475b31653e46d20cd09f9fdcf3b12bdacb4f5", "name": "VIU", "decimals": 18 },
  { "addr": "0x4470bb87d77b963a013db939be332f927f2b992e", "name": "ADX", "decimals": 4},
  { "addr": "0x4dc3643dbc642b72c158e7f3d2ff232df61cb6ce", "name": "AMB", "decimals": 18},
  { "addr": "0x960b236A07cf122663c4303350609A66A7B288C0", "name": "ANT", "decimals": 18},
  { "addr": "0x1a7a8bd9106f2b8d977e08582dc7d24c723ab0db", "name": "APPC", "decimals": 18},
  { "addr": "0xBA5F11b16B155792Cf3B2E6880E8706859A8AEB6", "name": "ARN", "decimals": 8},
  { "addr": "0x0d8775f648430679a709e98d2b0cb6250d2887ef", "name": "BAT", "decimals": 18},
  { "addr": "0xB8c77482e45F1F44dE1745F52C74426C631bDD52", "name": "BNB", "decimals": 18},
  { "addr": "0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c", "name": "BNT", "decimals": 18},
  { "addr": "0xcb97e65f07da24d46bcdd078ebebd7c6e6e3d750", "name": "BTM", "decimals": 8},
  { "addr": "0xf85feea2fdd81d51177f6b8f35f0e6734ce45f5f", "name": "CMT", "decimals": 18},
  { "addr": "0x41e5560054824ea6b0732e656e3ad64e20e94e45", "name": "CVC", "decimals": 8},
  { "addr": "0x0abdace70d3790235af448c88547603b945604ea", "name": "DNT", "decimals": 18},
  { "addr": "0xced4e93198734ddaff8492d525bd258d49eb388e", "name": "EDO", "decimals": 18},
  { "addr": "0xbf2179859fc6D5BEE9Bf9158632Dc51678a4100e", "name": "ELF", "decimals": 18},
  { "addr": "0xf629cbd94d3791c9250152bd8dfbdf380e2a3b9c", "name": "ENJ", "decimals": 18},
  { "addr": "0x5af2be193a6abca9c8817001f45744777db30756", "name": "ETHOS", "decimals": 8},
  { "addr": "0x419d0d8bdd9af5e606ae2232ed285aff190e711b", "name": "FUN", "decimals": 8},
  { "addr": "0x6810e776880c02933d47db1b9fc05908e5386b96", "name": "GNO", "decimals": 18},
  { "addr": "0xa74476443119A942dE498590Fe1f2454d7D4aC0d", "name": "GNT", "decimals": 18},
  { "addr": "0xc5bbae50781be1669306b9e001eff57a2957b09d", "name": "GTO", "decimals": 5},
  { "addr": "0x103c3a209da59d3e7c4a89307e66521e081cfdf0", "name": "GVT", "decimals": 18},
  { "addr": "0x888666CA69E0f178DED6D75b5726Cee99A87D698", "name": "ICN", "decimals": 18},
  { "addr": "0xb5a5f22694352c15b00323844ad545abb2b11028", "name": "ICX", "decimals": 18},
  { "addr": "0xfa1a856cfa3409cfa145fa4e20eb270df3eb21ab", "name": "IOST", "decimals": 18},
  { "addr": "0x5e6b6d9abad9093fdc861ea1600eba1b355cd940", "name": "ITC", "decimals": 18},
  { "addr": "0xdd974d5c2e2928dea5f71b9825b8b646686bd200", "name": "KNC", "decimals": 18},
  { "addr": "0x80fB784B7eD66730e8b1DBd9820aFD29931aab03", "name": "LEND", "decimals": 18},
  { "addr": "0x514910771af9ca656af840dff83e8264ecf986ca", "name": "LINK", "decimals": 18},
  { "addr": "0xfa05A73FfE78ef8f1a739473e462c54bae6567D9", "name": "LUN", "decimals": 18},
  { "addr": "0x0f5d2fb29fb7d3cfee444a200298f468908cc942", "name": "MANA", "decimals": 18},
  { "addr": "0xb63b606ac810a52cca15e44bb630fd42d8d1d83d", "name": "MCO", "decimals": 8},
  { "addr": "0xF433089366899D83a9f26A773D59ec7eCF30355e", "name": "MTL", "decimals": 8},
  { "addr": "0x5d65d971895edc438f465c17db6992698a52318d", "name": "NAS", "decimals": 18},
  { "addr": "0x2c4e8f2d746113d0696ce89b35f0d8bf88e0aeca", "name": "OST", "decimals": 18},
  { "addr": "0xB97048628DB6B661D4C2aA833e95Dbe1A905B280", "name": "PAY", "decimals": 18},
  { "addr": "0x9992ec3cf6a55b00978cddf2b27bc6882d88d1ec", "name": "POLY", "decimals": 18},
  { "addr": "0x595832f8fc6bf59c85c527fec3740a1b7a361269", "name": "POWR", "decimals": 6},
  { "addr": "0x618e75ac90b12c6049ba3b27f5d5f8651b0037f6", "name": "QASH", "decimals": 6},
  { "addr": "0xea26c4ac16d4a5a106820bc8aee85fd0b7b2b664", "name": "QKC", "decimals": 18},
  { "addr": "0x99ea4db9ee77acd40b119bd1dc4e33e1c070b80d", "name": "QSP", "decimals": 18},
  { "addr": "0x48f775efbe4f5ece6e0df2f7b5932df56823b990", "name": "R", "decimals": 0},
  { "addr": "0xf970b8e36e23f7fc3fd752eea86f8be8d83375a6", "name": "RCN", "decimals": 18},
  { "addr": "0x255aa6df07540cb5d3d297f0d0d4d84cb52bc8e6", "name": "RDN", "decimals": 18},
  { "addr": "0xe94327d07fc17907b4db788e5adf2ed424addff6", "name": "REP", "decimals": 18},
  { "addr": "0x8f8221afbb33998d8584a2b05749ba73c37a938a", "name": "REQ", "decimals": 18},
  { "addr": "0x4156D3342D5c385a87D264F90653733592000581", "name": "SALT", "decimals": 8},
  { "addr": "0x7c5a0ce9267ed19b22f8cae653f198e3e8daf098", "name": "SAN", "decimals": 18},
  { "addr": "0xb64ef51c888972c908cfacf59b47c1afbc0ab8ac", "name": "STORJ", "decimals": 8},
  { "addr": "0xd0a4b8946cb52f0661273bfbc6fd0e0c75fc6433", "name": "STORM", "decimals": 18},
  { "addr": "0x3883f5e181fccaF8410FA61e12b59BAd963fb645", "name": "THETA", "decimals": 18},
  { "addr": "0xf7920b0768ecb20a123fac32311d07d193381d6f", "name": "TNB", "decimals": 18},
  { "addr": "0xd850942ef8811f2a866692a623011bde52a462c1", "name": "VEN", "decimals": 18},
  { "addr": "0x39Bb259F66E1C59d5ABEF88375979b4D20D98022", "name": "WAX", "decimals": 8},
  { "addr": "0x05f4a42e251f2d52b8ed15e9fedaacfcef1fad27", "name": "ZIL", "decimals": 12},
  { "addr": "0xe41d2489571d322189246dafa5ebde1f4699f498", "name": "ZRX", "decimals": 18}
], item => tokens.set(item.name, {address: item.addr, decimals: item.decimals}));

app.get('/api/config', function (req, res) {
  var config = {
    supports_search: true,
    supports_group_request: false,
    supports_marks: false,
    supports_timescale_marks: false,
    supports_time: true,
    symbols_types: [
      {
        name: "Index",
        value: "index"
      }
    ],
    supported_resolutions: ['1', '5', '15', '30', '60', '1D', '1W', '1M']
  };

  res.write(JSON.stringify(config));
  res.end();
});

app.get('/api/time', function (req, res) {
  var now = new Date();
  res.write(Math.floor(now / 1000) + '');
  res.end();
});

app.get('/api/symbols', function (req, res) {

  var result = {
    "name": req.query.symbol,
    "exchange-traded": "ETHER20",
    "exchange-listed": "ETHER20",
    "timezone": "America/New_York",
    "minmov": 1,
    "minmov2": 0,
    "pointvalue": 1,
    "has_no_volume": false,
    "session": "24x7",
    "has_intraday": true,
    "has_weekly_and_monthly": true,
    "intraday_multipliers": ['1', '5', '15', '30', '60'],
    "description": req.query.symbol,
    "type": "index",
    "supported_resolutions": ['1', '5', '15', '30', '60', "D", "2D", "3D", "W", "3W", "M", "6M"],
    "pricescale": 1000000,
    "ticker": req.query.symbol
  };

  res.write(JSON.stringify(result));
  res.end();
});

app.get('/api/search', function (req, res) {

  const result = []
  let iter = tokens.keys()
  let el = iter.next()
  while(!el.done) {
    result.push({
      "symbol": el.value,
      "full_name": el.value,
      "description": "ERC20",
      "exchange": "ETHER20",
      "type": "index"
    })
    el = iter.next()
  }
  res.write(JSON.stringify(result));
  res.end();
});

app.get('/api/history', function (req, res) {

  const symbol = req.query.symbol
  const resolution = req.query.resolution
  const from = req.query.from
  const to = req.query.to


  const token = tokens.get(symbol)

  ev.getPricesByAddress(token.address, resolution, from, to, res);

});

app.listen(8888)