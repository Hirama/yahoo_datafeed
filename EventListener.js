const bn = require('bignumber.js')
const Web3 = require('web3')
const _ = require('lodash')
const web3 = new Web3(new Web3.providers.HttpProvider('https://mainnet.infura.io/wRAIg3KbD0yXgE89prjQ'))
const MongoClient = require('mongodb').MongoClient
const EXCHANGE_ADDRESS = "0xa39067521338b93ebC1B4ef6a8AF4832Dda6930b" // RPOD
const TOKEN_DECIMALS = "1E+"
const ETHER_DECIMALS = "1E+18"

const rs = require('./Resampler')

const abi = require('./abi.js')


const tokens = new Map()
_.each([
  { "addr": "0x519475b31653e46d20cd09f9fdcf3b12bdacb4f5", "name": "VIU", "decimals": 18 },
  { "addr": "0x4470bb87d77b963a013db939be332f927f2b992e", "name": "ADX", "decimals": 4},
  { "addr": "0x4dc3643dbc642b72c158e7f3d2ff232df61cb6ce", "name": "AMB", "decimals": 18},
  { "addr": "0x960b236A07cf122663c4303350609A66A7B288C0", "name": "ANT", "decimals": 18},
  { "addr": "0x1a7a8bd9106f2b8d977e08582dc7d24c723ab0db", "name": "APPC", "decimals": 18},
  { "addr": "0xBA5F11b16B155792Cf3B2E6880E8706859A8AEB6", "name": "ARN", "decimals": 8},
  { "addr": "0x0d8775f648430679a709e98d2b0cb6250d2887ef", "name": "BAT", "decimals": 18},
  { "addr": "0xB8c77482e45F1F44dE1745F52C74426C631bDD52", "name": "BNB", "decimals": 18},
  { "addr": "0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c", "name": "BNT", "decimals": 18},
  { "addr": "0xcb97e65f07da24d46bcdd078ebebd7c6e6e3d750", "name": "BTM", "decimals": 8},
  { "addr": "0xf85feea2fdd81d51177f6b8f35f0e6734ce45f5f", "name": "CMT", "decimals": 18},
  { "addr": "0x41e5560054824ea6b0732e656e3ad64e20e94e45", "name": "CVC", "decimals": 8},
  { "addr": "0x0abdace70d3790235af448c88547603b945604ea", "name": "DNT", "decimals": 18},
  { "addr": "0xced4e93198734ddaff8492d525bd258d49eb388e", "name": "EDO", "decimals": 18},
  { "addr": "0xbf2179859fc6D5BEE9Bf9158632Dc51678a4100e", "name": "ELF", "decimals": 18},
  { "addr": "0xf629cbd94d3791c9250152bd8dfbdf380e2a3b9c", "name": "ENJ", "decimals": 18},
  { "addr": "0x5af2be193a6abca9c8817001f45744777db30756", "name": "ETHOS", "decimals": 8},
  { "addr": "0x419d0d8bdd9af5e606ae2232ed285aff190e711b", "name": "FUN", "decimals": 8},
  { "addr": "0x6810e776880c02933d47db1b9fc05908e5386b96", "name": "GNO", "decimals": 18},
  { "addr": "0xa74476443119A942dE498590Fe1f2454d7D4aC0d", "name": "GNT", "decimals": 18},
  { "addr": "0xc5bbae50781be1669306b9e001eff57a2957b09d", "name": "GTO", "decimals": 5},
  { "addr": "0x103c3a209da59d3e7c4a89307e66521e081cfdf0", "name": "GVT", "decimals": 18},
  { "addr": "0x888666CA69E0f178DED6D75b5726Cee99A87D698", "name": "ICN", "decimals": 18},
  { "addr": "0xb5a5f22694352c15b00323844ad545abb2b11028", "name": "ICX", "decimals": 18},
  { "addr": "0xfa1a856cfa3409cfa145fa4e20eb270df3eb21ab", "name": "IOST", "decimals": 18},
  { "addr": "0x5e6b6d9abad9093fdc861ea1600eba1b355cd940", "name": "ITC", "decimals": 18},
  { "addr": "0xdd974d5c2e2928dea5f71b9825b8b646686bd200", "name": "KNC", "decimals": 18},
  { "addr": "0x80fB784B7eD66730e8b1DBd9820aFD29931aab03", "name": "LEND", "decimals": 18},
  { "addr": "0x514910771af9ca656af840dff83e8264ecf986ca", "name": "LINK", "decimals": 18},
  { "addr": "0xfa05A73FfE78ef8f1a739473e462c54bae6567D9", "name": "LUN", "decimals": 18},
  { "addr": "0x0f5d2fb29fb7d3cfee444a200298f468908cc942", "name": "MANA", "decimals": 18},
  { "addr": "0xb63b606ac810a52cca15e44bb630fd42d8d1d83d", "name": "MCO", "decimals": 8},
  { "addr": "0xF433089366899D83a9f26A773D59ec7eCF30355e", "name": "MTL", "decimals": 8},
  { "addr": "0x5d65d971895edc438f465c17db6992698a52318d", "name": "NAS", "decimals": 18},
  { "addr": "0x2c4e8f2d746113d0696ce89b35f0d8bf88e0aeca", "name": "OST", "decimals": 18},
  { "addr": "0xB97048628DB6B661D4C2aA833e95Dbe1A905B280", "name": "PAY", "decimals": 18},
  { "addr": "0x9992ec3cf6a55b00978cddf2b27bc6882d88d1ec", "name": "POLY", "decimals": 18},
  { "addr": "0x595832f8fc6bf59c85c527fec3740a1b7a361269", "name": "POWR", "decimals": 6},
  { "addr": "0x618e75ac90b12c6049ba3b27f5d5f8651b0037f6", "name": "QASH", "decimals": 6},
  { "addr": "0xea26c4ac16d4a5a106820bc8aee85fd0b7b2b664", "name": "QKC", "decimals": 18},
  { "addr": "0x99ea4db9ee77acd40b119bd1dc4e33e1c070b80d", "name": "QSP", "decimals": 18},
  { "addr": "0x48f775efbe4f5ece6e0df2f7b5932df56823b990", "name": "R", "decimals": 0},
  { "addr": "0xf970b8e36e23f7fc3fd752eea86f8be8d83375a6", "name": "RCN", "decimals": 18},
  { "addr": "0x255aa6df07540cb5d3d297f0d0d4d84cb52bc8e6", "name": "RDN", "decimals": 18},
  { "addr": "0xe94327d07fc17907b4db788e5adf2ed424addff6", "name": "REP", "decimals": 18},
  { "addr": "0x8f8221afbb33998d8584a2b05749ba73c37a938a", "name": "REQ", "decimals": 18},
  { "addr": "0x4156D3342D5c385a87D264F90653733592000581", "name": "SALT", "decimals": 8},
  { "addr": "0x7c5a0ce9267ed19b22f8cae653f198e3e8daf098", "name": "SAN", "decimals": 18},
  { "addr": "0xb64ef51c888972c908cfacf59b47c1afbc0ab8ac", "name": "STORJ", "decimals": 8},
  { "addr": "0xd0a4b8946cb52f0661273bfbc6fd0e0c75fc6433", "name": "STORM", "decimals": 18},
  { "addr": "0x3883f5e181fccaF8410FA61e12b59BAd963fb645", "name": "THETA", "decimals": 18},
  { "addr": "0xf7920b0768ecb20a123fac32311d07d193381d6f", "name": "TNB", "decimals": 18},
  { "addr": "0xd850942ef8811f2a866692a623011bde52a462c1", "name": "VEN", "decimals": 18},
  { "addr": "0x39Bb259F66E1C59d5ABEF88375979b4D20D98022", "name": "WAX", "decimals": 8},
  { "addr": "0x05f4a42e251f2d52b8ed15e9fedaacfcef1fad27", "name": "ZIL", "decimals": 12},
  { "addr": "0xe41d2489571d322189246dafa5ebde1f4699f498", "name": "ZRX", "decimals": 18}
], item => tokens.set(item.addr, item.decimals));

const resolutions = new Map()

resolutions.set('1', {count: 1, unit: 'minutes'})
resolutions.set('5', {count: 5, unit: 'minutes'})
resolutions.set('15', {count: 15, unit: 'minutes'})
resolutions.set('30', {count: 30, unit: 'minutes'})
resolutions.set('60', {count: 60, unit: 'minutes'})
resolutions.set('D', {count: 1, unit: 'days'})
resolutions.set('2D', {count: 2, unit: 'days'})
resolutions.set('3D', {count: 3, unit: 'days'})
resolutions.set('W', {count: 1, unit: 'weeks'})
resolutions.set('3W', {count: 3, unit: 'weeks'})
resolutions.set('M', {count: 1, unit: 'months'})
resolutions.set('6M', {count: 6, unit: 'months'})


const options = {
  useNewUrlParser: true
}

function storeBlock(db) {
  // Смотрим в базе последний обработанный блок
  // Берем следующий если есть, обробатываем и сохраняем в базу
  db.collection('blocks').find().limit(1).sort({$natural: -1}).toArray(async function (err, result) {
    if (result.length === 0) {
      result = []
      result.push({number: 5776204})
      console.log(result)
    }
    if (!err) {
      console.log("Last from db:", result[0].number)
      const current = await web3.eth.getBlockNumber()
      console.log("Current from blockchain:", current)

      if (result[0].number < current) {

        for (let i = result[0].number + 1; i <= current; i++) {
          console.log(i)
          try {
            const block = await web3.eth.getBlock(i)
            if (block) {
              console.log("Proceed block", block.number)
              await db.collection('blocks').save({number: i})
              const trades = await web3.eth.getPastLogs({
                address: EXCHANGE_ADDRESS,
                fromBlock: web3.utils.toHex(i),
                toBlock: web3.utils.toHex(i),
                topics: ["0x6effdda786735d5033bfad5f53e5131abcced9e52be6c507b62d639685fbed6d"]
              })

              const orders = await web3.eth.getPastLogs({
                address: EXCHANGE_ADDRESS,
                fromBlock: web3.utils.toHex(i),
                toBlock: web3.utils.toHex(i),
                topics: ["0x3F7F2EDA73683C21A15F9435AF1028C93185B5F1FA38270762DC32BE606B3E85"]
              })

              const timestamp = await blockToTime(block.number)

              for (let i = 0; i < trades.length; i++) {
                const tradeTick = await dataToParams(trades[i], timestamp)
                const trade = await unpackTrade(trades[i], timestamp)
                await db.collection(tradeTick.token).save(tradeTick)
                await db.collection("trades").save(trade)
              }

              for (let i = 0; i < orders.length; i++) {
                const order = await unpackOrder(orders[i], timestamp)
                await db.collection("orders").save(order)
              }
            }
          } catch (err) {
            console.log(err)
          }
          console.log('saved to database')
        }
      }

      setTimeout(storeBlock, 5000)
    }
  })
}

let Main = () => {
  MongoClient.connect('mongodb://node:DkUIgtGGdipi1pyXTwYv@ds145223.mlab.com:45223/ether', options, (err, client) => {
    if (err) return console.log(err)
    const db = client.db('ether') // whatever your database name is
    storeBlock(db)
  })
}

function getDataAddress(address, resolution, from, to, res) {
  MongoClient.connect('mongodb://node:DkUIgtGGdipi1pyXTwYv@ds145223.mlab.com:45223/ether', options, (err, client) => {
    if (err) return console.log(err)
    const db = client.db('ether') // whatever your database name is

    console.log(`getDataAddress. addr: ${address} from: ${from} to: ${to}`)
    db.collection(address.toLowerCase()).find({time: {$gt: +from, $lt: +to}}).toArray(function (err, results) {
      if (err) {
        console.log(`getDataAddress ERROR: ${err}`)
      }
      console.log(results)

      if (results.length !== 0) {
        const raw = rs.resample(results, resolutions.get(resolution))

        const converter = rs.toOHLC()

        const candles = _.map(raw, converter)

        const t = _.map(candles, item => item.t.unix())
        const o = _.map(candles, item => item.o)
        const h = _.map(candles, item => item.h)
        const l = _.map(candles, item => item.l)
        const c = _.map(candles, item => item.c)
        const v = _.map(candles, item => item.v)

        var data = {
          "t": t,
          "o": o,
          "h": h,
          "l": l,
          "c": c,
          "v": v,
          "s": "ok"
        }
      } else {
        var data = {
          "t": [],
          "o": [],
          "h": [],
          "l": [],
          "c": [],
          "v": [],
          "s": "ok"
        }
      }

      res.write(JSON.stringify(data));
      res.end();

    })

  })
}

async function unpackTrade(topic, timestamp) {
  const tokenGet = "0x" + topic.data.substring(26, 66)
  const amountGet = "0x" + topic.data.substring(66, 130)
  const tokenGive = "0x" + topic.data.substring(154, 194)
  const amountGive = "0x" + topic.data.substring(194, 258)

  if (tokenGive === "0x0000000000000000000000000000000000000000") { // sell
    const contract = new web3.eth.Contract(abi, tokenGet)
    const token_decimals = await contract.methods.decimals().call()
    const get = bn.BigNumber(amountGet).div(TOKEN_DECIMALS + token_decimals)
    const give = bn.BigNumber(amountGive).div(ETHER_DECIMALS)
    return {tokenGet, tokenGive, get: get.toString(), give: give.toString(), time: timestamp}
  } else {                                                          // buy
    const contract = new web3.eth.Contract(abi, tokenGive);
    const token_decimals = await contract.methods.decimals().call()
    const get = bn.BigNumber(amountGet).div(ETHER_DECIMALS)
    const give = bn.BigNumber(amountGive).div(TOKEN_DECIMALS + token_decimals)
    return {tokenGet, tokenGive, get: get.toString(), give: give.toString(), time: timestamp}
  }
}

async function unpackOrder(topic, timestamp) {
  const tokenGet = "0x" + topic.data.substring(26, 66)
  const amountGet = "0x" + topic.data.substring(66, 130)
  const tokenGive = "0x" + topic.data.substring(154, 194)
  const amountGive = "0x" + topic.data.substring(194, 258)
  const expires1 = "0x" + topic.data.substring(258, 322)
  const nonce1 = "0x" + topic.data.substring(322, 386)
  const user = "0x" + topic.data.substring(410, 452)

  // console.log("TOPIC_ORDER", topic.data)
  // console.log("ORDER PARSED", {
  //   tokenGet,
  //   amountGet: bn.BigNumber(amountGet).toNumber(),
  //   tokenGive,
  //   amountGive: bn.BigNumber(amountGive).toString(),
  //   expires: bn.BigNumber(expires).toString(),
  //   nonce: bn.BigNumber(nonce).toString(),
  //   user
  // })

  if (tokenGive === "0x0000000000000000000000000000000000000000") { // sell
    const contract = new web3.eth.Contract(abi, tokenGet)
    const token_decimals = await contract.methods.decimals().call()
    const get = bn.BigNumber(amountGet).div(TOKEN_DECIMALS + token_decimals)
    const give = bn.BigNumber(amountGive).div(ETHER_DECIMALS)
    const expires =  bn.BigNumber(expires1).toString()
    const nonce = bn.BigNumber(nonce1).toString()
    return {
      hash: web3.utils.soliditySha3(EXCHANGE_ADDRESS, tokenGet, amountGet, tokenGive, amountGive, expires1, nonce1),
      tokenGet,
      tokenGive,
      get: get.toString(),
      give: give.toString(),
      expires,
      nonce,
      user,
      time: timestamp
    }
  } else {                                                          // buy
    const contract = new web3.eth.Contract(abi, tokenGive);
    const token_decimals = await contract.methods.decimals().call()
    const get = bn.BigNumber(amountGet).div(ETHER_DECIMALS)
    const give = bn.BigNumber(amountGive).div(TOKEN_DECIMALS + token_decimals)
    const expires =  bn.BigNumber(expires1).toString()
    const nonce = bn.BigNumber(nonce1).toString()
    return {
      hash: web3.utils.soliditySha3(EXCHANGE_ADDRESS, tokenGet, amountGet, tokenGive, amountGive, expires1, nonce1),
      tokenGet,
      tokenGive,
      get: get.toString(),
      give: give.toString(),
      expires,
      nonce,
      user,
      time: timestamp
    }
  }
}

async function dataToParams(topic, timestamp) {

  const tokenGet = "0x" + topic.data.substring(26, 66)
  const amountGet = "0x" + topic.data.substring(66, 130)
  const tokenGive = "0x" + topic.data.substring(154, 194)
  const amountGive = "0x" + topic.data.substring(194, 258)

  if (tokenGive === "0x0000000000000000000000000000000000000000") { // sell
    const contract = new web3.eth.Contract(abi, tokenGet);
    const token_decimals = await contract.methods.decimals().call()
    const get = bn.BigNumber(amountGet).div(TOKEN_DECIMALS + token_decimals)
    const give = bn.BigNumber(amountGive).div(ETHER_DECIMALS)
    const price = give.div(get).toNumber()
    return {token: tokenGet, price, amount: give.toString(), time: timestamp}
  } else {                                                          // buy
    const contract = new web3.eth.Contract(abi, tokenGive);
    const token_decimals = await contract.methods.decimals().call()
    const get = bn.BigNumber(amountGet).div(ETHER_DECIMALS)
    const give = bn.BigNumber(amountGive).div(TOKEN_DECIMALS + token_decimals)
    const price = get.div(give).toNumber()
    return {token: tokenGive, price, amount: get.toString(), time: timestamp}
  }

}

const blockToTime = (item) => {
  return new Promise((resolve, reject) => {
    web3.eth.getBlock(item, (err, res) => {
      err ? reject(err) : resolve(res.timestamp)
    })
  })
}


module.exports = {"subscriber": Main, "getPricesByAddress": getDataAddress}