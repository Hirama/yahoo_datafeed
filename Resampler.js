const _ = require('lodash')
const bn = require('bignumber.js')
const moment = require('moment')

const resample = (series, interval) => {
  const startTime = moment.unix(series[0].time)
  const endTime = moment.unix(series[series.length - 1].time)
  let mark = startTime.clone().add(interval.count, interval.unit)
  const result = []
  let n = 0
  while (mark.isBefore(endTime)) {
    const sample = []
    while (n < series.length && moment.unix(series[n].time).isBefore(mark)) {
      sample.push(series[n])
      n++
    }
    result.push({time: mark.clone(), sample})
    mark.add(interval.count, interval.unit)
  }
  if (n < series.length) {
    const sample = []
    while (n < series.length) {
      sample.push(series[n])
      n++
    }
    result.push({time: endTime, sample})
  }
  return result
}

const toOHLC = () => {
  let lastPrice = 0
  return (part) => {
    const sample = part.sample
    if (sample.length > 0) {
      lastPrice = sample[sample.length - 1].price
      console.log(sample[0].amount)
      console.log(new bn.BigNumber(sample[0].amount))
      return {
        t: part.time,
        o: sample[0].price,
        h: _.maxBy(sample, item => item.price).price,
        l: _.minBy(sample, item => item.price).price,
        c: sample[sample.length - 1].price,
        v: _.reduce(sample, (sum, item) => sum.plus(new bn.BigNumber(item.amount)), new bn.BigNumber(0)).toNumber()
      }
    } else {
      return {t: part.time, o: lastPrice, h: lastPrice, l: lastPrice, c: lastPrice, v: 0}
    }
  }
}


module.exports = {"resample": resample, "toOHLC": toOHLC}
